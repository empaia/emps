---
emp: 0106
title: Asynchronous Job IO Locking and Optimization
authors: Christoph Jansen
editors: Bjoern Lindequist
type: modification
status: draft
---

# Asynchronous Job IO Locking and Optimization

[[_TOC_]]

## Introduction

Job IO Locking and Optimization in the EMPAIA platform is complex and time consuming. When Jobs are being run or finalized multiple steps must be performed subsequently in a single blocking request. Only when all the steps have been performed, the request returns a success message to the AI App or App UI and the HTTP request completes. The time of the IO processing depends on the complexity of inputs and outputs defined in the App's EAD, the performance of the platform service implementations, the DB performance impacted by the number of rows already stored in DB tables and the current system load. If the IO processing takes a long time, the calling HTTP client can run into a client-side timeout, that cannot be controlled by the platform. The EMPAIA API mostly avoids long, blocking HTTP requests for the most part, but did not yet achieve this in the context of IO processing. This EMP proposes an extension to the existing Medical Data v3 API spec to enable Asynchronous Job IO Locking and Optimization. The extension contains breaking changes in the communication between the EMPAIA core services (App Service and Workbench Daemon) and the Medical Data v3 API, but does not break the compatibility with existing AI Apps and App UIs.

## Specification

The following sections provide background information for understanding the problem with synchronous IO processing and specify the required API extensions and process changes to support Asynchronous Job IO Locking and Optimization.

### Background Information

The `PUT /{scope_id}/jobs/{job_id}/run` route of the Workbench v3 Scopes API performs the following steps.

1. Check if all inputs that are defined in the EAD have been created by the App (actually missing in the Workbench Service reference implementation).
2. For every input, call the `PUT /jobs/{job_id}/lock/{resource_type}/{resource_id}` route of the Medical Data v3 API (implemented by the Annotation Service).
3. If everything worked as expected, call the `PUT /jobs/{job_id}/status` route to set the Job status to `READY`.
4. Once the Job status is being set to `READY` the `PUT /jobs/{job_id}/status` route internally performs a long, blocking operation by calling a private route of the Annotation Service to create materialized views in the DB for query optimization reasons.

In this document we will refer to steps 1, 2 and 3 together as **Job Input Locking**, because these steps are part of the EMPAIA API specification and must be performed outside of the Medical Data v3 API by the core services (currently by the Workbench Service).

In this document we will refer to step 4 as **Job Input Optimization**. This step is an internal implementation detail of the Medical Data v3 API, specifically of the Job Service and Annotation Service. The changes proposed in this document will also allow for such internal processes to be implemented asynchronously.

The `PUT /{job_id}/finalize` route of the App v3 API performs the following steps.

1. Check if all outputs that are defined in the EAD have been created by the App.
2. For every output, call the `PUT /jobs/{job_id}/lock/{resource_type}/{resource_id}` route of the Medical Data v3 API (implemented by the Annotation Service).
3. If everything worked as expected, call the `PUT /jobs/{job_id}/status` route and set the Job status to `COMPLETED`.
4. Once the Job status is being set to `COMPLETED` the `PUT /jobs/{job_id}/status` route internally performs a long, blocking operation by calling a private route of the Annotation Service to create materialized views in the DB for query optimization reasons.

In this document we will refer to steps 1, 2 and 3 together as **Job Output Locking**. These steps are part of the EMPAIA API specification and must be performed outside of the Medical Data v3 API by the core services (currently by the App Service).

In this document we will refer to step 4 as **Job Output Optimization**. This step is an internal implementation detail of the Medical Data v3 API, specifically of the Job Service and Annotation Service. The changes proposed in this document will also allow for such internal processes to be implemented asynchronously.

### API Extensions

* Medical Data v3 API (Job Service)
  * New route: `PUT /jobs/{job_id}/io-status`
  * New optional URL query parameter: `GET /jobs?with_io_status=true`
  * New optional URL query parameter: `GET /jobs/{job_id}?with_io_status=true`
  * New optional URL query parameter: `PUT /jobs/{job_id}/query?with_io_status=true`
* Workbench v3 Scopes API (Workbench Service)
  * New optional URL query parameter: `GET /jobs?with_io_status=true`
  * New optional URL query parameter: `GET /jobs/{job_id}?with_io_status=true`
  * New optional URL query parameter: `PUT /jobs/{job_id}/run?non_blocking=true`
  * New optional URL query parameter: `PUT /jobs/{job_id}/finalize?non_blocking=true`

The API capabilities allow setting and getting the `io-status` of the Job. The `io-status` is separated from the Job status to maintain compatibility with existing App UIs and can be set to the following ENUM values (state changes occur in the given order):

* `NONE`
* `READY_INPUT_LOCKING` (set automatically, when Job status `READY` is set)
* `RUNNING_INPUT_LOCKING`
* `READY_INPUT_OPTIMIZATION`
* `RUNNING_INPUT_OPTIMIZATION`
* `READY_SCHEDULING`
* `WAITING` (set automatically, when Job status `SCHEDULED` or `RUNNING` is set)
* `READY_OUTPUT_LOCKING`
* `RUNNING_OUTPUT_LOCKING`
* `READY_OUTPUT_OPTIMIZATION`
* `RUNNING_OUTPUT_OPTIMIZATION`
* `COMPLETED` (set automatically, when Job status `COMPLETED` is set)

### Process Changes for Backend Jobs

Applies to modes `standalone`, `preprocessing` and `postprocessing (containerized=true)`.

#### Backend Job Input Locking

1. The Workbench v3 Scopes API route `PUT /jobs/{job_id}/run` will set the Job status `READY` and finish the request, providing a success response to the App UI's HTTP client. Internally the `io-status` is set to `READY_INPUT_LOCKING`.
2. The Workbench Daemon will detect the Job in status `READY` and that the `io-status` is `READY_INPUT_LOCKING`.
3. The Workbench Daemon will set the `io-status` to `RUNNING_INPUT_LOCKING` and start an asynchronous subroutine to perform the Job Input Locking. Changing the `io-status` prevents the Workbench Daemon to accidentally start another subroutine for the same job in the next iteration step of its main loop.
4. The subroutine checks if all intputs that are defined in the EAD have been created. If inputs have not been set it will set the Job status to `ERROR` and abort operations. If inputs have been set correctly, it will lock all inputs via `PUT /jobs/{job_id}/lock/{resource_type}/{resource_id}`. Once the subroutine is done with input locking it will set the `io-status` to `READY_INPUT_OPTIMIZATION`.

When the workbench daemon is starting up it checks if Jobs in status `READY` exist where `io-status` is `RUNNING_INPUT_LOCKING`. In such a case it will set the Job status to `ERROR`, because the system cannot recover from a partial Job Input Locking that has not been finished previously.

#### Backend Job Input Optimization

1. The internal daemon of the Medical Data Service tracks Jobs in status `READY` until the `io-status` is `READY_INPUT_OPTIMIZATION`.
2. The internal daemon will set the `io-status` to `RUNNING_INPUT_OPTIMIZATION` and start and asynchronous subroutine, that calls a private route of the Annotation Service to create materialized views in the DB.
3. Once the Job Input Optimization is done, the subroutine will set the `io-status` status to `READY_SCHEDULING`. If a problem occurs the Job status will instead be set to `ERROR`.
4. The Workbench Daemon will detect that the `io-status` is `READY_SCHEDULING` and schedule the Job for processing via the Job Exection Service as usual, changing the Job status to `SCHEDULED` and then `RUNNING`. Internally the `io-status` is set to `WAITING`.

When the internal daemon of the Medical Data Service is starting up it checks if Jobs in status `READY` exist where `io-status` is `RUNNING_INPUT_OPTIMIZATION`. In such a case it will set the Job status to `ERROR`, because the system cannot recover from a partial Job Input Optimization that has not been finished previously.

#### Backend Job Output Locking

1. The App v3 API route `PUT /{job_id}/finalize` will set the `io-status` to `READY_OUTPUT_LOCKING` and finish the request, providing a success response to the App's HTTP client. The App will terminate as usual.
2. The Workbench Daemon tracks the `io-status` of Jobs in status `RUNNING` until it detects that the `io-status` is `READY_OUTPUT_LOCKING`.
3. The Workbench Daemon will set the `io-status` to `RUNNING_OUTPUT_LOCKING` and start an asynchronous subroutine to perform the Job Output Locking. Changing the `io-status` prevents the Workbench Daemon to accidentally start another subroutine for the same job in the next iteration step of its main loop.
4. The subroutine checks if all outputs that are defined in the EAD have been created by the App. If outputs have not been set it will set the Job status to `ERROR` and abort operations. If outputs have been set correctly, it will lock all outputs via `PUT /jobs/{job_id}/lock/{resource_type}/{resource_id}`. Once the subroutine is done with output locking it will set the `io-status` to `READY_OUTPUT_OPTIMIZATION`.

When the workbench daemon is starting up it checks if Jobs in status `RUNNING` exist where `io-status` is `RUNNING_OUTPUT_LOCKING`. In such a case it will set the Job status to `ERROR`, because the system cannot recover from a partial Job Output Locking that has not been finished previously.

#### Backend Job Output Optimization

1. The internal daemon of the Medical Data Service tracks Jobs in status `RUNNING` until the `io-status` is `READY_OUTPUT_OPTIMIZATION`.
2. The internal daemon will set the `io-status` to `RUNNING_OUTPUT_OPTIMIZATION` and start and asynchronous subroutine, that calls a private route of the Annotation Service to create materialized views in the DB.
3. Once the Internal Job Output Optimization is done, the subroutine will set the Job status to `COMPLETED`. If a problem occurs the Job status will instead be set to `ERROR`.

When the internal daemon of the Medical Data Service is starting up it checks if Jobs in status `RUNNING` exist where `io-status` is `RUNNING_OUTPUT_OPTIMIZATION`. In such a case it will set the Job status to `ERROR`, because the system cannot recover from a partial Job Output Optimization that has not been finished previously.

### Process Changes for Frontend Jobs

Applies to modes `report` and `postprocessing (containerized=false)`, where `PUT /jobs/{job_id}/run` is never being called.

#### Frontend Job IO Locking and Optimization

1. When the App UI calls the Workbench v3 Scopes API route `PUT /jobs/{job_id}/finalize`, such that the Workbench Service sets the Job status to `READY`. Internally the `io-status` is set to `READY_INPUT_LOCKING`.
2. The Workbench Daemon will detect the Job in status `READY` and that the `io-status` is `READY_INPUT_LOCKING`.
3. The Workbench Daemon will set the `io-status` to `RUNNING_INPUT_LOCKING` and start an asynchronous subroutine to perform the Job Input Locking. Changing the `io-status` prevents the Workbench Daemon to accidentally start another subroutine for the same job in the next iteration step of its main loop.
4. The subroutine checks if all intputs that are defined in the EAD have been created. If inputs have not been set it will set the Job status to `ERROR` and abort operations. If inputs have been set correctly, it will lock all inputs via `PUT /jobs/{job_id}/lock/{resource_type}/{resource_id}`. Once the subroutine is done with input locking it will set the `io-status` to `READY_INPUT_OPTIMIZATION`.
5. The internal daemon of the Medical Data Service tracks Jobs in status `READY` until the `io-status` is `READY_INPUT_OPTIMIZATION`.
6. The internal daemon will set the `io-status` to `RUNNING_INPUT_OPTIMIZATION` and start and asynchronous subroutine, that calls a private route of the Annotation Service to create materialized views in the DB.
7. Once the Job Input Optimization is done, the internal daemon will set the Job status to `RUNNING` and then the `io-status` to `READY_OUTPUT_LOCKING` (skipping `READY_SCHEDULING`).
8. The Workbench Daemon tracks the `io-status` of Jobs in status `RUNNING` until it detects that the `io-status` is `READY_OUTPUT_LOCKING`.
9. The Workbench Daemon will set the `io-status` to `RUNNING_OUTPUT_LOCKING` and start an asynchronous subroutine to perform the Job Output Locking. Changing the `io-status` prevents the Workbench Daemon to accidentally start another subroutine for the same job in the next iteration step of its main loop.
10. The subroutine checks if all outputs that are defined in the EAD have been created by the App. If outputs have not been set it will set the Job status to `ERROR` and abort operations. If outputs have been set correctly, it will lock all outputs via `PUT /jobs/{job_id}/lock/{resource_type}/{resource_id}`. Once the subroutine is done with output locking it will set the `io-status` to `READY_OUTPUT_OPTIMIZATION`.
11. The internal daemon of the Medical Data Service tracks Jobs in status `RUNNING` until the `io-status` is `READY_OUTPUT_OPTIMIZATION`.
12. The internal daemon will set the `io-status` to `RUNNING_OUTPUT_OPTIMIZATION` and start and asynchronous subroutine, that calls a private route of the Annotation Service to create materialized views in the DB.
13. Once the Internal Job Output Optimization is done, the subroutine will set the Job status to `COMPLETED`. If a problem occurs the Job status will instead be set to `ERROR`.

#### Frontend Jobs Backwards Compatibility

App UIs that already exist and are not aware of the process changes can call the Workbench v3 Scopes API routes `PUT /jobs/{job_id}/run` and `PUT /jobs/{job_id}/finalize` as usual and the route will keep blocking until all Job IO Locking and Optimization steps are done. This is necessary, because the App UI expects the state changes and processes to be finished, for example to immediately follow with a data query request that relies on DB materialized views to be present.

New App UIs that are aware of the changes can call the Workbench v3 Scopes API routes `PUT /jobs/{job_id}/run?non-blocking=true` and `PUT /jobs/{job_id}/finalize?non-blocking=true`, such that the Workbench Service immediately finishes the requests, providing a success response to the App UI's HTTP client. The App UI will be able to poll `GET /jobs/{job_id}/io-status` for processing updates, while the platform performs the Asynchronous Job IO Locking and Optimization.

### Process Changes for Examination Closing

With the Asynchronous Job IO Locking and Optimization process the closing of examinations becomes a lot simpler, because only Job status updates must be performed in the route.

1. The Workbench Client calls the Workbench v3 API route `PUT /examinations/{examination_id}/close`.
2. For every backend Job in status `ASSEMBLY` the route sets the Job status to `ERROR`, because the user will not be able to finish the job assembly in a read-only Scope.
3. For every frontend Job in status `ASSEMBLY` the route sets the Job status to `READY`.
4. Frontend and backend Jobs that are still `RUNNING` will not be interrupted, but will finish in Job status `COMPLETE` or `ERROR` at a later point in time. This is handled by the asynchronous processing of the platform.
5. The route sets the Examination state to `CLOSED` (resulting in a read-only Scope) and finishes the request, providing a success response to the Workbench Client's HTTP client.

## Copyright

[CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, Christoph Jansen has waived all copyright and related or neighboring rights to Asynchronous Job IO Locking and Optimization. This work is published from: Germany.

### Additional license for source code included in this document

[MIT License](https://opensource.org/licenses/MIT)

Copyright 2024 Christoph Jansen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.