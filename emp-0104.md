---
emp: 0104
title: Multi-User Data Sharing in App UI Scopes
authors: Christoph Jansen
editors: Bjoern Lindequist
type: modification
status: approved
---

# Multi-User Data Sharing in App UI Scopes

[[_TOC_]]

## Introduction

The EMPAIA Platform supports clinical and research use cases alike. Especially in research settings it can be useful, if users can view annotations created by another user in the App UI of a specific App. One example is a tool for annotating research datasets, where the work is shared across multiple people. So far it was only possible to see another user's data if the data was an input of a job, because `/v3/scopes/{scope_id}/jobs` delivers all jobs (except for jobs that are in state `ASSEMBLY` and not created in the user's own scope) and the data queries all `jobs` as a filter option. This EMP defines a way for App UIs to explicitely fetch data created in other users' scopes.

## Specification

A new route `GET /v3/scopes/{scope_id}/scopes` is added to the Workbench v3 Scopes API. The route returns a list of Scope objects, that are part of the current Examination. The App UI can obtain the desired data in a three step process:

1. Fetch all Scope objects via the new API route.
2. From the list of objects create a flat list of Scope IDs.
3. Use the Scope ID list as a `{"creators": [...]}` filter in data queries (e.g. `PUT /v3/scopes/{scope_id}/annotations/query`).

## Implementation

The implementation will require the following changes:

* Add a new `PUT /v3/scopes/query` route with `examinations` and `scopes` filters to the Medical Data Service and the underlying Examination Service.
* Add a new `GET /v3/scopes/{scope_id}/scopes` to the Workbench Service, that internally uses the new Examination Service route.
* Allow all data query routes to include any Scope ID that is part of the current Examination in the `creators` filter.
* Add an optional Workbench Service environment setting, that disables multi-user data sharing, by applying the following measures:
    * `GET /v3/scopes/{scope_id}/scopes` only returns user's own Scope ID
    * `GET /v3/scopes/{scope_id}/jobs` only returns jobs created by the user's Scope, as well as all preprocessing jobs (which are always created outside the user's scope). Jobs created by other users' Scopes are being filtered out.
    * Change `creators` and `jobs` filters of all data queries to only allow IDs that are returned by the two previously described routes.

## Copyright

[CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, Christoph Jansen has waived all copyright and related or neighboring rights to Multi-User Data Sharing in App UI Scopes. This work is published from: Germany.

### Additional license for source code included in this document

[MIT License](https://opensource.org/licenses/MIT)

Copyright 2024 Christoph Jansen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
