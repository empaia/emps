from enum import Enum
from typing import Annotated, List, Literal, Optional, Union

from pydantic import UUID4, BaseModel, ConfigDict, Field, StrictInt


DATE_PATTERN = r"^\d\d\/\d\d\/\d\d\d\d$"
DATE_DESCRIPTION = "DD/MM/YYYY"


class YesNoType(str, Enum):
    YES = "YES"
    NO = "NO"


class MaleFemaleType(str, Enum):
    MALE = "MALE"
    FEMALE = "FEMALE"


class MenopausalStatusType(str, Enum):
    PRE = "PRE"
    PERI = "PERI"
    POST = "POST"


class VitalStateType(str, Enum):
    ALIVE = "ALIVE"
    DECEASED = "DECEASED"


class DrinkingStatusType(str, Enum):
    LIFETIME_ABSTAINER = "LIFETIME_ABSTAINER"
    FORMER_INFREQUENT_DRINKER = "FORMER_INFREQUENT_DRINKER"
    FORMER_REGULAR_DRINKER = "FORMER_REGULAR_DRINKER"
    CURRENT_INFREQUENT_DRINKER = "CURRENT_INFREQUENT_DRINKER"
    CURRENT_LIGHT_DRINKER = "CURRENT_LIGHT_DRINKER"
    CURRENT_MODERATE_DRINKER = "CURRENT_MODERATE_DRINKER"
    CURRENT_HEAVIER_DRINKER = "CURRENT_HEAVIER_DRINKER"


class RestrictedBaseModel(BaseModel):
    """Abstract Super-class not allowing unknown fields in the **kwargs."""

    model_config = ConfigDict(extra="forbid")


class Address(RestrictedBaseModel):
    street: Optional[str] = Field(default=None)
    number: Optional[str] = Field(default=None)
    postal_code: Optional[str] = Field(default=None)
    city: Optional[str] = Field(default=None)
    country: Optional[str] = Field(default=None)


class Physician(RestrictedBaseModel):
    name: Optional[str] = Field(default=None)
    address: Optional[Address] = Field(default=None)
    specialization: Optional[str] = Field(default=None)


class UniteStudy(RestrictedBaseModel):
    id: str = Field(description="study id (primary key)")
    type: Literal["unite_study"]
    sap_id: Optional[str] = Field(default=None)
    gtds_id: Optional[str] = Field(default=None)
    hu_token: Optional[str] = Field(default=None)
    consent_given: Optional[YesNoType] = Field(default=None)
    broad_consent: Optional[YesNoType] = Field(default=None)
    consent_details: Optional[str] = Field(default=None)
    address: Optional[Address] = Field(default=None)
    phone_number: Optional[str] = Field(default=None)
    email_address: Optional[str] = Field(default=None)
    primary_care_physician: Optional[Physician] = Field(default=None)
    surname: Optional[str] = Field(default=None)
    first_name: Optional[str] = Field(default=None)
    date_of_birth: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    gender: Optional[MaleFemaleType] = Field(default=None)
    biological_sex_at_birth: Optional[MaleFemaleType] = Field(default=None)
    race_ethnicity: Optional[str] = Field(
        default=None, description="https://www.nih.gov/nih-style-guide/race-national-origin"
    )
    last_known_vital_state: Optional[VitalStateType] = Field(default=None)
    date_of_last_known_vital_state: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    date_of_last_inhouse_contact: Optional[str] = Field(default=None, description="info")
    date_of_death: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    cause_of_death: Optional[str] = Field(default=None, description="info")
    smoking_status: Optional[str] = Field(default=None, description="PY")
    quitting_date_smoking: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    drinking_status: Optional[DrinkingStatusType] = Field(
        default=None, description="https://archive.cdc.gov/#/details?url=https://www.cdc.gov/nchs/nhis/alcohol/alcohol_glossary.htm"
    )
    time_to_next_treatment_months: Optional[StrictInt] = Field(default=None)
    current_clinical_trial_participation: Optional[YesNoType] = Field(default=None)
    current_clinical_trial_details: Optional[str] = Field(default=None)


PostUniteStudyItem = UniteStudy
UniteStudyItem = UniteStudy


class UniteStudyList(RestrictedBaseModel):
    item_count: int
    items: List[UniteStudyItem]


class UniteStudyRelated(RestrictedBaseModel):
    study_id: str


class PostUniteVisit(UniteStudyRelated):
    type: Literal["unite_visit"]
    date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    visit_details: Optional[str] = Field(default=None, examples=["follow up", "random", "whatever", "test"])


class PostUniteTrialParticipationHistory(UniteStudyRelated):
    trial_name: Optional[str] = Field(default=None)
    consent_given: Optional[YesNoType] = Field(default=None)
    trial_start_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class UniteVisit(PostUniteVisit):
    id: UUID4


class UniteTrialParticipationHistory(PostUniteTrialParticipationHistory):
    id: UUID4


PostUniteBureaucraticInformationItem = Union[PostUniteVisit, PostUniteTrialParticipationHistory]


UniteBureaucraticInformationItem = Union[UniteVisit, PostUniteTrialParticipationHistory]


class UniteBureaucraticInformationList(RestrictedBaseModel):
    item_count: int
    items: List[UniteBureaucraticInformationItem]


class PostUniteWeight(UniteStudyRelated):
    type: Literal["unite_weight"]
    weight: str = Field(description="kg or kg.g")
    date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteHeight(UniteStudyRelated):
    type: Literal["unite_height"]
    height: str = Field(description="m.cm")
    date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteBMI(UniteStudyRelated):
    type: Literal["unite_bmi"]
    bmi: str
    date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteEcogPerformanceStatus(UniteStudyRelated):
    type: Literal["unite_ecog_performance_status"]
    ecog_performance_status: StrictInt
    date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteSubstance(UniteStudyRelated):
    type: Literal["unite_substance"]
    substance: str = Field(examples=["penicllin", "novalgin", "ciprofloxacin", "wespengift"])
    reaction_type: str = Field(examples=["rash", "ausschlag", "anaphylaxie", "anaphylaxie"])


class PostUniteMenopausalStatus(UniteStudyRelated):
    type: Literal["unite_menopausal_status"]
    menopausal_status: MenopausalStatusType
    date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class UniteWeight(PostUniteWeight):
    id: UUID4


class UniteHeight(PostUniteHeight):
    id: UUID4


class UniteBMI(PostUniteBMI):
    id: UUID4


class UniteEcogPerformanceStatus(PostUniteEcogPerformanceStatus):
    id: UUID4


class UniteSubstance(PostUniteSubstance):
    id: UUID4


class UniteMenopausalStatus(PostUniteMenopausalStatus):
    id: UUID4


PostUniteCorePatientCharateristicsItem = Union[
    PostUniteWeight,
    PostUniteHeight,
    PostUniteBMI,
    PostUniteEcogPerformanceStatus,
    PostUniteSubstance,
    PostUniteMenopausalStatus,
]


UniteCorePatientCharateristicsItem = Union[
    UniteWeight,
    UniteHeight,
    UniteBMI,
    UniteEcogPerformanceStatus,
    UniteSubstance,
    UniteMenopausalStatus,
]


class UniteCorePatientCharateristicsList(RestrictedBaseModel):
    item_count: int
    items: List[UniteCorePatientCharateristicsItem]


class PostUniteRelativeDescription(UniteStudyRelated):
    type: Literal["unite_relative_description"]
    relative_description: Optional[str] = Field(
        default=None,
        description="mother / maternal grandmother / brother etc",
        examples=["mother", "brother", "sister", "maternal aunt"],
    )
    disease: Optional[str] = Field(
        default=None,
        examples=["ovarian cancer", "pca", "mammaca", "pancreasca"],
    )


class PostUniteKnownGermlineMutation(UniteStudyRelated):
    type: Literal["unite_known_germline_mutation"]
    known_germline_mutation: str = Field(
        examples=["braca1", "TP53", "APC Mutation", "braca2"],
    )


class PostUniteKnownSomaticMutation(UniteStudyRelated):
    type: Literal["unite_known_somatic_mutation"]
    known_somatic_mutation: str = Field(
        examples=["BRAF V600E Mutation", "KRAS Mutation", "PIK3CA", "unknown", "TMPRSS2-ERG", "TMPRSS2-ERG"],
    )


class PostUniteOtherMedicalHistory(UniteStudyRelated):
    type: Literal["unite_other_medical_history"]
    other_medical_history: str = Field(
        description="as not included in CCI",
        examples=["Z.n. Hysterektomie 2011", "Z.n. Hüft-Tep 2023", "Z.n. Appendektomie", "Z.n. CABG", "Z.n. PPPD"],
    )


class UniteRelativeDescription(PostUniteRelativeDescription):
    id: UUID4


class UniteKnownGermlineMutation(PostUniteKnownGermlineMutation):
    id: UUID4


class UniteKnownSomaticMutation(PostUniteKnownSomaticMutation):
    id: UUID4


class UniteOtherMedicalHistory(PostUniteOtherMedicalHistory):
    id: UUID4


PostUniteRiskFactorItem = Union[
    PostUniteRelativeDescription,
    PostUniteKnownGermlineMutation,
    PostUniteKnownSomaticMutation,
    PostUniteOtherMedicalHistory,
]


UniteRiskFactorItem = Union[
    UniteRelativeDescription,
    UniteKnownGermlineMutation,
    UniteKnownSomaticMutation,
    UniteOtherMedicalHistory,
]


class UniteRiskFactorList(RestrictedBaseModel):
    item_count: int
    items: List[UniteRiskFactorItem]


class PostUnitePrimaryDiagnosis(UniteStudyRelated):
    type: Literal["unite_primary_diagnosis"]
    physician: Optional[Physician] = Field(default=None)
    current_malignancy: Optional[str] = Field(
        default=None,
        description="one or more",
        examples=["pca", "nzk", "pancreasca", "colonca"]
    )
    method_of_primary_diagnosis: Optional[str] = Field(
        default=None,
        examples=["biopsy", "ct", "mri"]
    )
    age_at_primary_diagnosis: Optional[str] = Field(
        default=None,
        examples=["31 years, 10 months", "39 years, 0 months"]
    )
    primary_diagnosis_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUnitePriorMalignancy(UniteStudyRelated):
    type: Literal["unite_prior_malignancy"]
    prior_malignancy: str = Field(
        examples=["ALL in der Kindheit", "Osteochondrom in der Jugend", "Z.n. NZK mit Nephrektomie", "keine", "Z.n. Menigeom"]
    )


class PostUniteCurrentMedication(UniteStudyRelated):
    type: Literal["unite_current_medication"]
    current_medications_active_ingredient: str = Field(
        examples=["ASS", "Metformin", "Amlodipin", "Ramipril", "Rivaroxaban"]
    )
    medication_dose: Optional[float] = Field(default=None)
    medication_unit: Optional[str] = Field(
        default=None,
        examples=["mg"]
    )
    medication_interval: Optional[str] = Field(
        default=None,
        examples=["1-0-0"]
    )


class PostUniteBiopsy(UniteStudyRelated):
    type: Literal["unite_biopsy"]
    biopsy_results_gleason: Optional[str] = Field(
        default=None,
        description="[a+b]=c"
    )
    biopsy_results_histological_cell_type: Optional[str] = Field(
        default=None,
        description="adeno, cribiform, mixed, etc"
    )
    biopsy_results_isup_grade: Optional[str]
    biological_sample_id: Optional[str] = Field(
        default=None,
        description="Auftragsnummer"
    )
    biopsy_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteImagingResult(UniteStudyRelated):
    type: Literal["unite_imaging_result"]
    imaging_results_type: Optional[str] = Field(
        default=None,
        examples=["ct", "mri", "pet"]
    )
    metastasis_findings: Optional[str] = Field(default=None)
    recurrance_findings: Optional[str] = Field(default=None)
    imaging_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    m1_diagnosis_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    recurrence_diagnosis_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUnitePsa(UniteStudyRelated):
    type: Literal["unite_psa"]
    psa_value: str
    psa_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteDru(UniteStudyRelated):
    type: Literal["unite_dru"]
    dru_finding: str
    dru_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUnitePvol(UniteStudyRelated):
    type: Literal["unite_pvol"]
    pvol_mri: Optional[str] = Field(default=None)
    mri_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    pvol_trus: Optional[str] = Field(default=None)
    trus_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteInitialSymptom(UniteStudyRelated):
    type: Literal["unite_initial_symptom"]
    initial_symtpom: str


class PostUniteScores(UniteStudyRelated):
    type: Literal["unite_scores"]
    ipss_score: Optional[float] = Field(default=None)
    ipss_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    iief_score: Optional[float] = Field(default=None)
    iief_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    epic_score: Optional[float] = Field(default=None)
    epic_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteMolecularTest(UniteStudyRelated):
    type: Literal["unite_molecular_test"]
    molecular_test_type: Optional[str] = Field(default=None)
    molecular_test_date: Optional[str] = Field(default=None)
    material_or_tissue_tested: Optional[str] = Field(default=None)
    mutations_found: Optional[List[str]] = Field(default=None)


class UnitePrimaryDiagnosis(PostUnitePrimaryDiagnosis):
    id: UUID4


class UnitePriorMalignancy(PostUnitePriorMalignancy):
    id: UUID4


class UniteCurrentMedication(PostUniteCurrentMedication):
    id: UUID4


class UniteBiopsy(PostUniteBiopsy):
    id: UUID4


class UniteImagingResult(PostUniteImagingResult):
    id: UUID4


class UnitePsa(PostUnitePsa):
    id: UUID4


class UniteDru(PostUniteDru):
    id: UUID4


class UnitePvol(PostUnitePvol):
    id: UUID4


class UniteInitialSymptom(PostUniteInitialSymptom):
    id: UUID4


class UniteScores(PostUniteScores):
    id: UUID4


class UniteMolecularTest(PostUniteMolecularTest):
    id: UUID4


PostUniteOncologicHistoryItem = Union[
    PostUnitePrimaryDiagnosis,
    PostUnitePriorMalignancy,
    PostUniteCurrentMedication,
    PostUniteBiopsy,
    PostUniteImagingResult,
    PostUnitePsa,
    PostUniteDru,
    PostUnitePvol,
    PostUniteInitialSymptom,
    PostUniteScores,
    PostUniteMolecularTest,
]


UniteOncologicHistoryItem = Union[
    UnitePrimaryDiagnosis,
    UnitePriorMalignancy,
    UniteCurrentMedication,
    UniteBiopsy,
    UniteImagingResult,
    UnitePsa,
    UniteDru,
    UnitePvol,
    UniteInitialSymptom,
    UniteScores,
    UniteMolecularTest,
]


class UniteOncologicHistoryList(RestrictedBaseModel):
    item_count: int
    items: List[UniteOncologicHistoryItem]


class PostUniteLabValues(UniteStudyRelated):
    type: Literal["unite_lab_values"]
    testosterone_levels: Optional[float] = Field(default=None, description="ng dL")
    biomarkers: Optional[str] = Field(default=None)
    calcium_corrected: Optional[float] = Field(default=None, description="mg dL")
    hemoglobin: Optional[float] = Field(default=None, description="g dL")
    neutrophils: Optional[float] = Field(default=None, description="10^9 L")
    thrombocytes: Optional[float] = Field(default=None, description="10^9 L")
    complete_blood_count: Optional[StrictInt]
    white_blood_cell_count: Optional[StrictInt] = Field(default=None, description="10^9 L")
    platelets: Optional[float] = Field(default=None, description="10^9 L")
    lymphocytes: Optional[float] = Field(default=None, description="10^9 L")
    absolute_neutrophil_count: Optional[StrictInt] = Field(default=None, description="10^9 L")
    creatinine: Optional[float] = Field(default=None, description="mg dL")
    urea: Optional[float] = Field(default=None, description="mg dL")
    egfr: Optional[float] = Field(default=None, description="eGFR mL min 1.73m2")
    creatinine_clearance: Optional[float] = Field(default=None, description="mL min")
    ast: Optional[float] = Field(default=None, description="U L")
    alt: Optional[float] = Field(default=None, description="U L")
    bilirubin: Optional[float] = Field(default=None, description="mg dL")
    alkaline_phosphatase: Optional[float] = Field(default=None, description="U L")
    albumin: Optional[float] = Field(default=None, description="g dL")
    sodium: Optional[float] = Field(default=None, description="mEq L")
    potassium: Optional[float] = Field(default=None, description="mEq L")
    calcium: Optional[float] = Field(default=None, description="mg dL")
    magnesium: Optional[float] = Field(default=None, description="mg dL")
    phosphorus: Optional[float] = Field(default=None, description="mg dL")
    crp: Optional[float] = Field(default=None, description="CRP mg dL")
    date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class UniteLabValues(PostUniteLabValues):
    id: UUID4


class UniteLabValuesList(RestrictedBaseModel):
    item_count: int
    items: List[UniteLabValues]


class PostUniteAdjuvantTherapy(UniteStudyRelated):
    type: Literal["unite_adjuvant_therapy"]
    adjuvant_therapy_name: Optional[str] = Field(default=None)
    start_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    end_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)


class PostUniteMedicalTherapy(UniteStudyRelated):
    type: Literal["unite_medical_therapy"]
    medical_therapy_type: Optional[str] = Field(default=None)
    start_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    end_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    line_of_therapy: Optional[str] = Field(default=None)
    molecule_generic_name: Optional[str] = Field(default=None)
    dosage: Optional[str] = Field(default=None, description="mg")
    response: Optional[str] = Field(default=None)
    adverse_events: Optional[str] = Field(default=None)
    event_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    event_type: Optional[str] = Field(default=None)


class PostUniteSurgery(UniteStudyRelated):
    type: Literal["unite_surgery"]
    surgery_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, patten=DATE_PATTERN)
    procedresponseure: Optional[str] = Field(default=None)
    procedure_code: Optional[str] = Field(default=None)
    line_oresponsef_therapy: Optional[str] = Field(default=None)
    margin_status: Optional[str] = Field(default=None)
    lymph_node_dissection: Optional[str] = Field(default=None)
    number_of_lymph_nodes_removed: Optional[StrictInt] = Field(default=None)
    number_of_positive_lymph_nodes: Optional[StrictInt] = Field(default=None)
    lymphovascular_invasion: Optional[str] = Field(default=None)
    resection_status: Optional[str] = Field(default=None)
    complete_tnm_of_the_specimen: Optional[str] = Field(default=None)
    clavien_dindo_complications: Optional[str] = Field(default=None)


class PostUniteRadiotherapy(UniteStudyRelated):
    type: Literal["unite_radiotherapy"]
    start_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    end_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    procedure: Optional[str] = Field(default=None)
    line_of_therapy: Optional[str] = Field(default=None)
    radiotherapy_dose: Optional[str] = Field(default=None, description="Gy")
    fractions: Optional[str] = Field(default=None)


class PostUniteNuclearMedicalTherapy(UniteStudyRelated):
    type: Literal["unite_nuclear_medical_therapy"]
    nuclear_medical_therapy_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    start_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    end_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    procedure: Optional[str] = Field(default=None)
    line_of_therapy: Optional[str] = Field(default=None)


class PostUniteFocalTherapy(UniteStudyRelated):
    type: Literal["unite_focal_therapy"]
    focal_therapy: Optional[str] = Field(default=None)
    details_date: Optional[str] = Field(default=None, description=DATE_DESCRIPTION, pattern=DATE_PATTERN)
    response: Optional[str] = Field(default=None)


class UniteAdjuvantTherapy(PostUniteAdjuvantTherapy):
    id: UUID4


class UniteMedicalTherapy(PostUniteMedicalTherapy):
    id: UUID4


class UniteSurgery(PostUniteSurgery):
    id: UUID4


class UniteRadiotherapy(PostUniteRadiotherapy):
    id: UUID4


class UniteNuclearMedicalTherapy(PostUniteNuclearMedicalTherapy):
    id: UUID4


class UniteFocalTherapy(PostUniteFocalTherapy):
    id: UUID4


PostUniteTherapyItem = Union[
    PostUniteAdjuvantTherapy,
    PostUniteMedicalTherapy,
    PostUniteSurgery,
    PostUniteRadiotherapy,
    PostUniteNuclearMedicalTherapy,
    PostUniteFocalTherapy,
]


UniteTherapyItem = Union[
    UniteAdjuvantTherapy,
    UniteMedicalTherapy,
    UniteSurgery,
    UniteRadiotherapy,
    UniteNuclearMedicalTherapy,
    UniteFocalTherapy,
]


class UniteTherapyList(RestrictedBaseModel):
    item_count: int
    items: List[UniteTherapyItem]


class UniteQuery(RestrictedBaseModel):
    studies: Optional[Annotated[List[str], Field(min_length=1)]]
    cases: Optional[Annotated[List[str], Field(min_length=1)]]
    types: Optional[Annotated[List[str], Field(min_length=1)]]


class PostUniteStudyCaseMapping(UniteStudyRelated):
    type: Literal["unit_study_case_mapping"]
    case_id: str


class UniteStudyCaseMapping(PostUniteStudyCaseMapping):
    id: UUID4


class UniteStudyCaseMappingList(RestrictedBaseModel):
    item_count: int
    items: List[UniteStudyCaseMapping]
