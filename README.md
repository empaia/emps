# EMPAIA Mod Proposals (EMP)

EMP Editors:

* [Bjoern Lindequist](https://gitlab.com/bjoern.lindequist)
* [Christoph Jansen](https://gitlab.com/cjansenempaia)

List of EMPs:

| EMP                  | Name                                         | Type         | Status   |
| -------------------- | -------------------------------------------- | ------------ | -------- |
| [0001](emp-0001.md#) | EMP Purpose and Guidelines                   | moderation   | approved |
| [0100](emp-0100.md#) | Case Data Partitioning                       | modification | ready    |
| [0101](emp-0101.md#) | Adopting DICOMweb                            | modification | draft    |
| [0102](emp-0102.md#) | Pixel Maps                                   | modification | approved |
| [0103](emp-0103.md#) | App Requirements                             | modification | ready    |
| [0104](emp-0104.md#) | Multi-User Data Sharing in App UI Scopes     | modification | approved |
| [0105](emp-0105.md#) | FHIR Questionnaires for Structured Reporting | modification | draft    |
| [0106](emp-0106.md#) | Asynchronous Job IO Locking and Optimization | modification | draft    |
| [0107](emp-0107.md#) | Pixelmaps Tile Transfer Compression          | modification | approved |
| [0108](emp-0108.md#) | Pixelmaps Level and Resolution Info          | modification | approved |
| [0109](emp-0109.md#) | Variant Call Format                          | modification | draft    |
| [0110](emp-0110.md#) | UNITE Study Data                             | modification | draft    |
